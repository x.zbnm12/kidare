import React from 'react'

function LeagueEvent() {
  return (
    <div className="LeagueEvent_Wrapper flex flex-row bg-sky-100 px-4 justify-between py-1 items-center">
      <div className="LeagueEvent_TextWrapper">
        <div className="LeagueEvent_TitleWrapper">
          <p className="LeagueEvent_Title IRANSansRegular text-[10px]" >ورژن جدید در دسترس است</p>
        </div>
      </div>
      <div className="LeagueEvent_CloseWrapper">
        <img src='img/download.png' alt='download' width="20" height="20"/>
      </div>
    </div>
  )
}

export default LeagueEvent