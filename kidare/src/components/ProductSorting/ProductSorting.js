const title_sort = 'جدید ترین درخواست ها'

function ProductSorting() {
  return (
    <div className='ProductSorting_ProductSortingWrapper py-4 px-4 flex flex-row items-center justify-between'>
      <div className="FiltersWrapper flex flex-row items-center">
        <img className="" src='img/arrow.png' alt='download' width="14" height="20"/>
        <span className="mr-1 text-[10px] IRANSansRegular text-gray-400">{title_sort}</span>
      </div>
      <div className="ViewWrapper flex flex-row items-center">
        <span href="">
          <img className="ml-1" src='img/row.png' alt='download' width="17" height="20"/>
        </span>
        <span href="">
          <img className="" src='img/grid.png' alt='download' width="12" height="20"/>
        </span>
      </div>
    </div>
  )
}

export default ProductSorting