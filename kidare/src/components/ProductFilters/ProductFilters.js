

function ProductFilters() {
  return (
    <div className='ProductFilter_ProductFilterWrapper flex flex-row justify-start px-4 py-2 border-b-[0.5px] border-gray-100'>
      <div className="ProductFilter">
      <button
        type="button"
        className="flex flex-row justify-between items-center inline-block rounded border-[1px] border-slate-100 px-2 py-1 ml-4 text-[10px] IRANSansRegular leading-normal text-neutral-800 transition duration-150 ease-in-out hover:border-neutral-800 hover:bg-neutral-500 hover:bg-opacity-10 hover:text-neutral-800 focus:border-neutral-800 focus:text-neutral-800 focus:outline-none focus:ring-0 active:border-neutral-900 active:text-neutral-900 dark:border-neutral-900 dark:text-neutral-900 dark:hover:border-neutral-900 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10 dark:hover:text-neutral-900 dark:focus:border-neutral-900 dark:focus:text-neutral-900 dark:active:border-neutral-900 dark:active:text-neutral-900"
        data-te-ripple-init>
        فیلتر ها
        <img className="mr-3" src='img/filter.svg' alt='download' width="10" height="10"/>
      </button>
      </div>
      <div className="ProductFilter_Selected flex flex-row">
        <button
          type="button"
          className="flex flex-row justify-between items-center inline-block rounded border-[1px] border-slate-100 px-2 py-1 ml-4 text-[10px] IRANSansRegular leading-normal text-neutral-800 transition duration-150 ease-in-out hover:border-neutral-800 hover:bg-neutral-500 hover:bg-opacity-10 hover:text-neutral-800 focus:border-neutral-800 focus:text-neutral-800 focus:outline-none focus:ring-0 active:border-neutral-900 active:text-neutral-900 dark:border-neutral-900 dark:text-neutral-900 dark:hover:border-neutral-900 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10 dark:hover:text-neutral-900 dark:focus:border-neutral-900 dark:focus:text-neutral-900 dark:active:border-neutral-900 dark:active:text-neutral-900"
          data-te-ripple-init>
          مشهد
          <img className="mr-3" src='img/close.svg' alt='download' width="10" height="10"/>
        </button>
        <button
          type="button"
          className="flex flex-row justify-between items-center inline-block rounded border-[1px] border-slate-100 px-2 py-1 ml-4 text-[10px] IRANSansRegular leading-normal text-neutral-800 transition duration-150 ease-in-out hover:border-neutral-800 hover:bg-neutral-500 hover:bg-opacity-10 hover:text-neutral-800 focus:border-neutral-800 focus:text-neutral-800 focus:outline-none focus:ring-0 active:border-neutral-900 active:text-neutral-900 dark:border-neutral-900 dark:text-neutral-900 dark:hover:border-neutral-900 dark:hover:bg-neutral-100 dark:hover:bg-opacity-10 dark:hover:text-neutral-900 dark:focus:border-neutral-900 dark:focus:text-neutral-900 dark:active:border-neutral-900 dark:active:text-neutral-900"
          data-te-ripple-init>
           برند انکر
          <img className="mr-3" src='img/close.svg' alt='download' width="10" height="10"/>
        </button>
      </div>
    </div>
  )
}

export default ProductFilters