const itemsNavbar = [
  {
    title: 'درخواست ها',
    img_url: 'img/nav/home.png'
  },
  {
    title: 'مشتریان',
    img_url: 'img/nav/user.png'
  },
  {
    title: 'پیام ها',
    img_url: 'img/nav/chat.png'
  },
  {
    title: 'فروشگاه من',
    img_url: 'img/nav/store.png'
  },

]
function Navbar() {
  return (
    <div className='NavBar_NavBarWrapper'>
      <div className="Navigation_ItemsWrapper bg-white flex flex-row items-center w-full h-[70px] border-t-[1px] border-gray-100 fixed bottom-0">
        {
          itemsNavbar.map((item) => {
            const {title ,img_url} = item
            return (
              <div className='Navigation_NavItem basis-1/4'>
                <span href="#" aria-current="page" className="">
                  <div className="flex flex-col items-center">
                    <img className="Navigation_Icon" src={img_url} alt='home' width="20" height="20"/>
                    <p className='Navigation_Title text-[10px] IRANSansRegular pt-1'>{title}</p>
                  </div>
                </span>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}

export default Navbar