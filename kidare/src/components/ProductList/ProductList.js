import { products } from '../../data/products.js'
import ProductCard from '../ProductCard/ProductCard.js'
function ProductList() {
  
  return (
    <div className='ProductList_ProductListWrapper overflow-y-auto h-full my-28'>
      {
        products.map((product) => {
          return(
            <div key={product.id}>
              <ProductCard product={product} />
            </div>
          )
        })
      }
    </div>
  )
}

export default ProductList