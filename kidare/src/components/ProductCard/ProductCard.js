
function ProductCard(props) {
  const {img ,title ,brand ,date} = props.product
  return (
    <div className='ProductCard_ProductCardWrapper border-b-[1px] border-gray-100'>
      <div
        className="flex flex-row rounded-lg bg-white p-4 justify-between md:max-w-xl ">
        <div className="flex flex-col justify-between">
          <div>
            <h5
            className="mb-2 text-base text-slate-950  IRANSansMedium text-neutral-800">
            {title}
            </h5>
            <p className="mb-4 text-sm text-slate-600 IRANSansRegular">
              برند {brand}
            </p>
          </div>
          <p className="text-xs text-gray-400 IRANSansRegular">
            {date}
          </p>
        </div>
        <img
          className="h-28 w-[37%] rounded-lg object-cover md:h-20 md:w-48 md:rounded-none md:rounded-l-lg"
          src={img}
          alt=""
        />
      </div>
    </div>
  )
}

export default ProductCard