
import ProductFilters from '../components/ProductFilters/ProductFilters.js';
import ProductSorting from '../components/ProductSorting/ProductSorting.js';
import Navbar from '../components/Navbar/Navbar.js';
import LeagueEvent from '../components/LeagueEvent/LeagueEvent.js';
import ProductList from '../components/ProductList/ProductList.js';
function Home() {
  return (
    <>
      <div className='fixed top-0 w-full z-10 bg-white'>
        <LeagueEvent />
        <ProductFilters />
        <ProductSorting />
      </div>
        <ProductList className='z-0'/>
        <Navbar />
    </>
    
  )
}

export default Home